package Controllers

import (
	"SEEN-TECH-VAI21-BACKEND-GO/DBManager"
	"SEEN-TECH-VAI21-BACKEND-GO/Models"
	"SEEN-TECH-VAI21-BACKEND-GO/Utils"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"sync"

	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func SettingNew(c *fiber.Ctx) error {
	collection := DBManager.SystemCollections.Setting
	var self Models.Setting
	c.BodyParser(&self)
	res, err := collection.InsertOne(context.Background(), self)
	if err != nil {
		c.Status(500)
		return err
	}
	response, _ := json.Marshal(res)
	c.Status(200).Send(response)
	return nil
}

func settingGetAll(self *Models.SettingSearch) ([]bson.M, error) {
	collection := DBManager.SystemCollections.Setting
	var results []bson.M
	b, results := Utils.FindByFilter(collection, self.GetSettingSearchBSONObj())
	if !b || len(results) <= 0 {
		return results, errors.New("no settings object found")
	}
	return results, nil
}

func SettingGetAll(c *fiber.Ctx) error {
	var self Models.SettingSearch
	c.BodyParser(&self)
	results, err := settingGetAll(&self)
	if err != nil {
		c.Status(500)
		return err
	}
	response, _ := json.Marshal(bson.M{"result": results})
	c.Set("Content-Type", "application/json")
	c.Status(200).Send(response)
	return nil
}

func InitializeSetting() bool {
	collection := DBManager.SystemCollections.Setting
	_, results := Utils.FindByFilter(collection, bson.M{})
	if len(results) <= 0 { //no settings is initialized
		var self Models.Setting
		self.ID = primitive.NewObjectID()
		self.ProductSerial = 1
		_, err := collection.InsertOne(context.Background(), self)
		if err != nil {
			return false
		}
		fmt.Println("Initializing Setting Is Done")
	}
	return true
}

func SettingIncreasePurchaseSerial() error {
	// get setting value
	settingRes, settingErr := settingGetAll(&Models.SettingSearch{})
	if settingErr != nil {
		return settingErr
	}
	byteArray, _ := json.Marshal(settingRes[0])
	var setting Models.Setting
	json.Unmarshal(byteArray, &setting)

	// set setting value
	collectionSetting := DBManager.SystemCollections.Setting
	updateData := bson.M{
		"$set": bson.M{
			"purchaseserial": setting.PurchaseSerial + 1,
		},
	}
	_, updateErr := collectionSetting.UpdateOne(context.Background(), bson.M{"_id": setting.ID}, updateData)
	if updateErr != nil {
		return errors.New("an error occurred when Incrementing Purchasing Delivery Serial Number")
	} else {
		return nil
	}
}

// declaration of mutex
var mt sync.Mutex

func SettingIncreaseSalesSerial(Type string) error {
	// lock the mutex value
	mt.Lock()
	settingRes, settingErr := settingGetAll(&Models.SettingSearch{})
	if settingErr != nil {
		return settingErr
	}
	byteArray, _ := json.Marshal(settingRes[0])
	var setting Models.Setting
	json.Unmarshal(byteArray, &setting)

	collectionSetting := DBManager.SystemCollections.Setting
	updateData := bson.M{}

	// set setting value according the sales type
	if Type == "dismissalnotice" {
		updateData = bson.M{
			"$set": bson.M{
				"salesdimissalnoticeserial": setting.SalesDimissalNoticeSerial + 1,
			},
		}
	} else if Type == "quotation" {
		updateData = bson.M{
			"$set": bson.M{
				"salesquotationserial": setting.SalesQuotationSerial + 1,
			},
		}
	} else if Type == "order" {
		updateData = bson.M{
			"$set": bson.M{
				"salesorderserial": setting.SalesOrderSerial + 1,
			},
		}
	} else if Type == "delivery" {
		updateData = bson.M{
			"$set": bson.M{
				"salesdeliveryserial": setting.SalesDeliverySerial + 1,
			},
		}
	}

	_, updateErr := collectionSetting.UpdateOne(context.Background(), bson.M{"_id": setting.ID}, updateData)
	// unlock the mutex value
	mt.Unlock()
	if updateErr != nil {

		return errors.New("an error occurred when Incrementing Sales Serial Number")
	} else {

		return nil
	}
}

func SettingIncreaseProductSerial() error {
	// get setting value
	settingRes, settingErr := settingGetAll(&Models.SettingSearch{})
	if settingErr != nil {
		return settingErr
	}
	byteArray, _ := json.Marshal(settingRes[0])
	var setting Models.Setting
	json.Unmarshal(byteArray, &setting)

	// set setting value
	collectionSetting := DBManager.SystemCollections.Setting
	updateData := bson.M{
		"$set": bson.M{
			"productserial": setting.ProductSerial + 1,
		},
	}
	_, updateErr := collectionSetting.UpdateOne(context.Background(), bson.M{"_id": setting.ID}, updateData)
	if updateErr != nil {
		return errors.New("an error occurred when Incrementing Serial Number")
	} else {
		return nil
	}
}

func SettingIncreaseSalesReturningSerial() error {
	// get setting value
	settingRes, settingErr := settingGetAll(&Models.SettingSearch{})
	if settingErr != nil {
		return settingErr
	}
	byteArray, _ := json.Marshal(settingRes[0])
	var setting Models.Setting
	json.Unmarshal(byteArray, &setting)
	// set setting value
	collectionSetting := DBManager.SystemCollections.Setting
	updateData := bson.M{
		"$set": bson.M{
			"salesreturningserial": setting.SalesReturningSerial + 1,
		},
	}
	_, updateErr := collectionSetting.UpdateOne(context.Background(), bson.M{"_id": setting.ID}, updateData)
	if updateErr != nil {

		return errors.New("an error occurred when Incrementing Sales Serial Number")
	} else {

		return nil
	}
}

func SettingIncreaseJournalEntrySerial() error {
	// get setting value
	settingRes, settingErr := settingGetAll(&Models.SettingSearch{})
	if settingErr != nil {
		return settingErr
	}
	byteArray, _ := json.Marshal(settingRes[0])
	var setting Models.Setting
	json.Unmarshal(byteArray, &setting)

	// set setting value
	collectionSetting := DBManager.SystemCollections.Setting
	updateData := bson.M{
		"$set": bson.M{
			"journalentryserial": setting.JournalEntrySerial + 1,
		},
	}
	_, updateErr := collectionSetting.UpdateOne(context.Background(), bson.M{"_id": setting.ID}, updateData)
	if updateErr != nil {
		return errors.New("an error occurred when Incrementing Serial Number")
	} else {
		return nil
	}
}

func SettingGetPurchaseSerial() (int, error) {
	settingRes, settingErr := settingGetAll(&Models.SettingSearch{})
	if settingErr != nil {
		return -1, settingErr
	}
	byteArray, _ := json.Marshal(settingRes[0])
	var setting Models.Setting
	json.Unmarshal(byteArray, &setting)
	return setting.PurchaseSerial, nil
}

func SettingGetSalesSerial(Type string) (int, error) {
	settingRes, settingErr := settingGetAll(&Models.SettingSearch{})
	if settingErr != nil {
		return -1, settingErr
	}
	byteArray, _ := json.Marshal(settingRes[0])
	var setting Models.Setting
	json.Unmarshal(byteArray, &setting)

	// return sales serial number according the sales type
	if Type == "dismissalnotice" {
		return setting.SalesDimissalNoticeSerial, nil
	} else if Type == "quotation" {
		return setting.SalesQuotationSerial, nil
	} else if Type == "order" {
		return setting.SalesOrderSerial, nil
	} else if Type == "delivery" {
		return setting.SalesDeliverySerial, nil
	}
	return 0, nil
}

func SettingGetSalesReturningSerial() (int, error) {
	settingRes, settingErr := settingGetAll(&Models.SettingSearch{})
	if settingErr != nil {
		return -1, settingErr
	}
	byteArray, _ := json.Marshal(settingRes[0])
	var setting Models.Setting
	json.Unmarshal(byteArray, &setting)
	return setting.SalesReturningSerial, nil
}

func SettingGetProductSerial() (int, error) {
	settingRes, settingErr := settingGetAll(&Models.SettingSearch{})
	if settingErr != nil {
		return -1, settingErr
	}
	byteArray, _ := json.Marshal(settingRes[0])
	var setting Models.Setting
	json.Unmarshal(byteArray, &setting)
	return setting.ProductSerial, nil
}
func SettingGetJournalEntrySerial() (int, error) {
	settingRes, settingErr := settingGetAll(&Models.SettingSearch{})
	if settingErr != nil {
		return -1, settingErr
	}
	byteArray, _ := json.Marshal(settingRes[0])
	var setting Models.Setting
	json.Unmarshal(byteArray, &setting)
	return setting.JournalEntrySerial, nil
}
