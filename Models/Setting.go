package Models

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Setting struct {
	ID                        primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	ProductSerial             int                `json:"productserial,omitempty"`
	PurchaseSerial            int                `json:"purchaseserial,omitempty"`
	PurchasingDeliverySerial  int                `json:"purchasingdeliveryserial,omitempty"`
	JournalEntrySerial        int                `json:"journalentryserial,omitempty"`
	SalesReturningSerial      int                `json:"salesreturningserial,omitempty"`
	SalesQuotationSerial      int                `json:"salesquotationserial,omitempty"`
	SalesOrderSerial          int                `json:"salesorderserial,omitempty"`
	SalesDeliverySerial       int                `json:"salesdeliveryserial,omitempty"`
	SalesDimissalNoticeSerial int                `json:"salesdimissalnoticeserial,omitempty"`
}

type SettingSearch struct {
	IDIsUsed                        bool               `json:"idisused,omitempty" bson:"idisused,omitempty"`
	ID                              primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	ProductSerial                   int                `json:"productserial,omitempty"`
	ProductSerialIsUsed             bool               `json:"productserialisused,omitempty"`
	PurchaseSerial                  int                `json:"purchaseserial,omitempty"`
	PurchaseSerialIsUsed            bool               `json:"purchaseserialisused,omitempty"`
	PurchasingDeliverySerial        int                `json:"purchasingdelivery,omitempty"`
	PurchasingDeliverySerialIsUsed  bool               `json:"purchasingdeliveryserialisused,omitempty"`
	JournalEntrySerial              int                `json:"journalentry,omitempty"`
	JournalEntrySerialIsUsed        bool               `json:"journalentryserialisused,omitempty"`
	SalesReturningSerial            int                `json:"salesreturningserial,omitempty"`
	SalesReturningSerialIsUsed      bool               `json:"salesreturningserialisused,omitempty"`
	SalesQuotationSerial            int                `json:"salesquotationserial,omitempty"`
	SalesQuotationSerialIsUsed      bool               `json:"salesquotationserialisused,omitempty"`
	SalesOrderSerial                int                `json:"salesorderserial,omitempty"`
	SalesOrderSerialIsUsed          bool               `json:"salesordersserialisused,omitempty"`
	SalesDeliverySerial             int                `json:"salesdeliveryserial,omitempty"`
	SalesDeliverySerialIsUsed       bool               `json:"salesdeliveryserialisused,omitempty"`
	SalesDimissalNoticeSerial       int                `json:"salesdimissalnoticeserial,omitempty"`
	SalesDimissalNoticeSerialIsUsed bool               `json:"salesdimissalnoticeserialisused,omitempty"`
}

func (obj Setting) GetIdString() string {
	return obj.ID.String()
}

func (obj Setting) GetId() primitive.ObjectID {
	return obj.ID
}

func (obj SettingSearch) GetSettingSearchBSONObj() bson.M {
	self := bson.M{}
	if obj.IDIsUsed {
		self["_id"] = obj.ID
	}

	if obj.ProductSerialIsUsed {
		self["productserial"] = obj.ProductSerial
	}

	if obj.PurchaseSerialIsUsed {
		self["purchaseserial"] = obj.PurchaseSerial
	}

	if obj.PurchasingDeliverySerialIsUsed {
		self["purchasingdeliveryserial"] = obj.PurchasingDeliverySerial
	}

	if obj.JournalEntrySerialIsUsed {
		self["journalentryserial"] = obj.JournalEntrySerial
	}

	if obj.SalesReturningSerialIsUsed {
		self["salesreturningserial"] = obj.SalesReturningSerial
	}

	// new added
	if obj.SalesQuotationSerialIsUsed {
		self["salesquotationserial"] = obj.SalesQuotationSerial
	}
	if obj.SalesOrderSerialIsUsed {
		self["salesorderserial"] = obj.SalesOrderSerial
	}
	if obj.SalesDeliverySerialIsUsed {
		self["salesdeliveryserial"] = obj.SalesDeliverySerial
	}
	if obj.SalesDimissalNoticeSerialIsUsed {
		self["salesdimissalnoticeserial"] = obj.SalesDimissalNoticeSerial
	}

	return self
}
